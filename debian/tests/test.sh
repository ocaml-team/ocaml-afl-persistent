#!/bin/sh

if ! command -v ocamlopt > /dev/null; then
   echo "not on a native architecture, skipping tests"
   exit 0
fi

cd test
exec ./test.sh
